.. include:: /helpers/roles.rst

.. rst-class:: creature-details

***********************************************************************************************************
Рыцари Ластволла (`Knights of Lastwall <https://2e.aonprd.com/MonsterFamilies.aspx?ID=201>`_)
***********************************************************************************************************

**Источник**: Lost Omens: Character Guide pg. 122

Хотя государство Ластволл разрушена, а ее лидеры бежали, мужество тех, кто когда-то защищал ее сияющие стены, остается сильным.
:ref:`Рыцари Ластволла <Faction--Knights-of-Lastwall>` появляются в местах великих бедствий, сопровождая мирных жителей и отбиваясь от нежити.
Если игровые персонажи когда-нибудь отправятся в зону кризиса, они, скорее всего, увидят там и Рыцарей Ластволла, независимо от того, формируют ли они бронированную стену для защиты невинных или пробираются через смертоносные пустоши, где даже смелейшие ощущают смертельный страх.
Если игровые персонажи не являются злыми или невольными жертвами агентов Тар-Бафона - :ref:`Шепчущего Пути <faith--Whispering-Way>`, то эти рыцари в основном служат ценными союзниками в землях, где дружественные лица редки.

:doc:`Добродетельный защитник <Virtuous-Defender>` может быть членом рыцарского отряда или вербовщиком, ищущим тех, кто готов рискнуть своей жизнью, чтобы спасти Авистан от ужасной опасности.
:doc:`Ветеран восстановитель <Veteran-Reclaimer>`, скорее всего, находится в глубинах Могильных Земель или других действительно заброшенных мест, где даже малейшая ошибка может привести к смерти или к еще более худшей участи.
Такие хитрые, но добродетельные души могут попросить помочь привести людей в безопасное место, выступить в роли проводника по незнакомой территории или дать ценный совет и информацию об опасных существах или ловушках, с которыми могут столкнуться игровые персонажи.



.. toctree::
   :glob:
   :maxdepth: 3
   
   *
