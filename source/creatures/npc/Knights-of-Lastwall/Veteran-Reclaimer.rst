.. include:: /helpers/roles.rst

.. rst-class:: creature-details

Ветеран восстановитель (Veteran Reclaimer)
============================================================================================================

.. sidebar:: |lore|

	.. rubric:: Руталь Айронрор (Ruthal Ironroar)

	**Источник**: Lost Omens: Character Guide pg. 123

	Постоянные сражения между Белкзеном и Ластволлом привели к взаимным предрассудкам, и Руталь никогда не считала приятной свою жизнь ни в одной из этих стран.
	Много лет Руталь жила одна в глуши, избегая других.
	Но когда Ластволл стал рушиться, полуорчиха обнаружила, что не может отвернуться.
	Хотя ее настроение капризно, а социальные навыки плохи, Руталь - настоящий ангел для тех, кого она спасает.


Когда Ластволл пал, многие из бывших Рыцарей Озэма впали в уныние и рассеялись, бесцельно блуждая по дебрям Внутреннего Моря.
Хотя ужасные события, приведшие к падению Ластволла, поколебали их веру в традиционную иерархию Рыцарей, они укрепили свою решимость стереть всю нежить с лица Голариона.
Эти Багровые Восстановители по-прежнему считают Шепчущего Тирана своим главным врагом, но их можно встретить во многих самых истерзанных регионах мира, живущих за счет того, что дает земля и охотящихся на неупокоенных мертвецов.

В то время как героизм и рвение к своему делу все еще горят в их сердцах, ветераны восстановители прожили достаточно долго, чтобы утратить вкус к рассказам о придворной доблести и доблестных последних рубежах.
Они усвоили ценность осторожности и поэтому вступают в бой со своими врагами с расстояния и используют местность для получения преимущества.
Они быстро наносят ответный удар врагам, причиняющим вред их союзникам, зная, что перед лицом множества ужасов мира, необходимы верные товарищи по команде.



.. rst-class:: creature
.. _bestiary--Veteran-Reclaimer:

Ветеран восстановитель (`Veteran Reclaimer <https://2e.aonprd.com/NPCs.aspx?ID=970>`_) / Существо 11
------------------------------------------------------------------------------------------------------------

- :alignment:`ХД`
- :size:`средний`
- орк
- человек
- гуманоид

**Источник**: Lost Omens: Character Guide pg. 123

**Восприятие**: +24; ночное зрение

**Языки**: Всеобщий,
Орочий

**Навыки**:
Акробатика +20,
Атлетика +19,
Запугивание +15,
Медицина +19,
Природа +18,
Религия +20,
Выживание +22

**Сил** +4,
**Лвк** +5,
**Тел** +4,
**Инт** +0,
**Мдр** +3,
**Хар** +0


**Предметы**:
*разящий композитный длинный лук +1* (100 стрел),
*разящий меч-бастард +1*,
кинжал,
*нагрудник +1*,
:ref:`Зелье исцеления, малое (Healing potion, lesser) <item--Healing-Potion>`

----------

**КБ**: 29;
**Стойкость**: +20,
**Рефлекс**: +23 (успехи становятся крит.успехами),
**Воля**: +19

**ОЗ**: 195

----------

**Скорость**: 25 футов;
игнорирует эффекты немагической сложной местности


**Ближний бой**: |д-1| *меч-бастард* +21 [+16/+11] (:w_two_hand:`двуручное d12`, :t_magical:`магический`),
**Урон** 2d8+8 рубящий

**Ближний бой**: |д-1| кинжал +21 [+17/+13] (:w_agile:`быстрое`, :w_versatile:`универсальное Р`),
**Урон** 1d4+8 колющий

**Дальний бой**: |д-1| *композитный длинный лук* +22 [+17/+12] (:w_deadly:`смертельное 1d10`, :w_propulsive:`тяговое`, шаг дистанции 100 футов, перезарядка 0, :w_volley:`залповое 30 фт`, :t_magical:`магический`)
**Урон** 2d8+6 колющий

**Дальний бой**: |д-1| кинжал +21 [+17/+13] (:w_agile:`быстрое`, :w_thrown:`метательное 10 фт`, :w_versatile:`универсальное Р`),
**Урон** 1d4+8 колющий


**Багровая месть (Crimson Vengeance)** |д-1|
**Частота**: раз в раунд;
**Требования**: Ветеран восстановитель владеет оружием дальнего боя с перезарядкой 0;
**Эффект**: Ветеран восстановитель совершает два :ref:`Удара (Strikes) <action--Strike>` требуемым оружием по цели, которая в течение последнего раунда атаковала его или одного из его союзников (штраф множественной атаки применяется как обычно).
Если они оба попадают в существо, объедините их урон с целью преодоления сопротивлений и слабостей.


**Природное мастерство (Nature's Edge)**: Враги становятся :c_flat_footed:`застигнуты врасплох` для ветерана восстановителя в областях с естественной сложной местностью или областях, ставших сложной местностью из-за :t_snare:`силка`.

**Мстительное преимущество (Vengeful Edge)**: При атаке существа, которое атаковало ветерана восстановителя или одного из его союзников за прошедший раунд, ветеран восстановитель имеет штраф множественной атаки равный -3, или -2 для кинжала.

**Мастерство оружия (Weapon Mastery)**: Когда ветеран восстановитель совершает критическое попадание, по существу, которое за прошедший раунд атаковало его или одного из его союзников, восстановитель применяет эффект критической специализации оружия.





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst