.. include:: /helpers/roles.rst

.. rst-class:: creature-details

***********************************************************************************************************
Гремлин (`Gremlin <https://2e.aonprd.com/MonsterFamilies.aspx?ID=58>`_)
***********************************************************************************************************

**Источник**: Bestiary pg. 192,
Bestiary 2 pg. 134,
Bestiary 3 pg. 120

Гремлины - это жестокие феи трикстеры и диверсанты, которые полностью приспособились к жизни на :ref:`Материальном Плане <plane--Material>`, нашедшие определенные ниши для своей изобретательной разрушительной деятельности.
Все гремлины любят разрушать или ломать вещи, будь то что-то физическое, как устройство или транспортное средство, так и что-то неосязаемое, как союз или отношения.
Самая большая радость для гремлина - наблюдать за тем, как рушатся комплексные творения, желательно после легчайшего и незначительного, тщательно спланированного толчка от гремлина.
Гремлины склонны принижать, задирать и даже убивать своих меньших сородичей, особенно митфлитов, которых гремлины (и многие другие) уничижительно называют "клопами (mites)".

Вредные феи, известные как гремлины, будто имеют бесконечное множество разновидностей, отличающихся друг от друга внешним видом.
Предпочитая разрушать, а не создавать, гремлины почти никогда не строят собственных жилищ, заселясь в туннели и заброшенные здания.
Свои логова они дополняют жестокими ловушками, а иногда и дрессированными животными (часто теми, которых гуманоиды считают вредителями).
Логова гремлинов обычно находится рядом с городом или деревней (будь то на поверхности или в Темноземье), которые обеспечивают их готовой пищей, материалами для создания ловушек и, конечно, жертвами для развлечения.

Внешние различия между самцами и самками гремлинов незначительны, и многие наблюдатели ошибочно полагают, что они однополы или бесполы.
Гремлины очень плодовиты, и их горстка может быстро превратиться в толпу.
Молодых гремлинов воспитывают сообща, из-за необходимости, поскольку они с рождения стремятся причинять вред.
Они быстро растут и полностью вырастают за несколько недель.

Гремлины давным-давно возникли в :ref:`Первом Мире <plane--First-World>`, живым воплощением способности природы изнашиваться, разрушаться и разлагаться.
На Материальном Плане их столкновения со смертными цивилизациями превратили их в существ, преданных хаосу, вредительству и западням, причем каждая разновидность специализируется на конкретном виде беспорядка.



.. toctree::
   :glob:
   :maxdepth: 3
   
   *



.. rst-class:: h3
.. rubric:: Колокольчики от гремлинов (Gremlin Bells)

|lore|

**Источник**: Bestiary pg. 192

В суеверных обществах иногда вешают крошечные колокольчики из полудрагоценных металлов, полагая, что они отговорят гремлинов от разрушения прикрепленного к нему предмета или проникновения в дом.
Как ни странно, большинство гремлинов тоже верят в это суеверие, и даже если колокольчик от гремлинов не был магически зачарован, гремлин обычно не рискует возиться с объектами, которые кажутся защищенными таким образом.


.. rst-class:: h3
.. rubric:: Гремлинские "сокровища" (Gremlin "Treasure")

|treasure|

**Источник**: Bestiary pg. 193

Все гремлины собирают клады, и их гнезда загромождены как ценными, так и бесполезными вещами.
Разбирая гнездо гремлинов, можно обнаружить неожиданные сокровища, такие как украшения или мелкие магические предметы, но нужно быть осторожным, чтобы не порезаться о ржавые куски металла, не подобрать :doc:`проклятые предметы </game_mastering/tools/Cursed-Items>` или не потревожить скрытое гнездо ядовитых паразитов.


.. rst-class:: h3
.. rubric:: Гремлины-миньоны (Gremlin Minions)

|lore|

**Источник**: Bestiary 2 pg. 134

Большинство знает, что лучше не нанимать гремлинов, поэтому, когда эти феи живут бок о бок с другими, они часто являются паразитами и нежеланными гостями.
Изобретательные и злобные люди, обнаружившие у себя нашествие гремлинов, иногда ловят их и выпускают в домах своих врагов.


.. rst-class:: h3
.. rubric:: Вера гремлинов (Gremlin Faith)

|lore|

**Источник**: Bestiary 2 pg. 135

Принципиально-злые гремлины иногда тяготеют к поклонению :ref:`архидьяволам <Deities--Other--Archdevils>`, хотя и не в ортодоксальной манере.
:doc:`/lost_omens/Deity/Archdevil/Dispater` почитается как архидьявол городов - их площадок для воровства и саботажа.
:doc:`Маммону </lost_omens/Deity/Archdevil/Mammon>` поклоняются как приносящему богатства и защитнику гремлинских нор, которые, как и его владения Эреб, темны и полны ловушек.
Нейтрально-злые гремлины поклоняются :doc:`Норгорберу </lost_omens/Deity/Core/Norgorber>` в его аспекте покровителя воров, но :doc:`пагвамы <Pugwampi>` особенно предпочитают поклоняться :doc:`гноллам </creatures/bestiary/Gnoll/index>` (или, по крайней мере, тому, кому поклоняются местные гноллы).
Хаотично-злые гремлины часто почитают Андерифку (Andirifkhu), демоническую повелительницу иллюзий, ножей и ловушек.
Те, кто сохранил связь с :ref:`Первым Миром <plane--First-World>`, могут поклясться в верности :doc:`Королю Фонарей </lost_omens/Deity/Eldest/The-Lantern-King>`, Старейшему смеха, озорства и трансмутации.


.. rst-class:: h3
.. rubric:: Общество гремлинов (Gremlin Society)

|lore|

**Источник**: Pathfinder #169: Kindled Magic pg. 82

Большинство сообществ гремлинов эгалитарны и действующие согласованно, не придающие особо значения семейным узам по сравнению с узами сообщества в целом.
Лидерами гремлинов часто становятся самые молодые и надоедливые члены группы, которые получают свое положение добиваясь отставки старого лидера или запугивая других гремлинов, чтобы те подчинились.
Взрослые гремлины поощряют детей к самостоятельным исследованиям, надеясь, что те будут заняты преследованием местных смертных, а не созданием проблем для других гремлинов.


.. rst-class:: h3
.. rubric:: Питомцы гремлинов (Gremlin Pets)

|creatures|

**Источник**: Pathfinder #169: Kindled Magic pg. 83

Гремлины, устраивающие свои гнезда рядом с домами смертных, неизбежно сталкиваются с домашними животными.
Большинство гремлинов могут использовать свои врожденные магические способности для общения с животными, что позволяет им договариваться с домашними и рабочими животными для помощи в терроре, воровстве или их обучении, чтобы они досаждали своим хозяевам.
Особенно гремлины любят дружить с кошками, которых они редко беспокоят, привлекая их к своим шалостям и затеям в качестве соучастников.





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst