.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Гаруда (Garuda)
============================================================================================================

.. sidebar:: |lore|

	.. rubric:: Змеиная ненависть (Serpentine Hatred)
	
	**Источник**: Bestiary 3 pg. 104
	
	В далеком прошлом мать всех гаруд была хитростью обращена в рабство злой королевой змей.
	Сильнейшие из потомков гаруд отправились в великое путешествие и преодолели множество опасных препятствий, чтобы освободить свою мать - хитростью, которая одурачила даже умную королеву.
	С тех пор гаруды - смертельные враги :doc:`наг </creatures/bestiary/Naga/index>`, злых змей и других аберрантных змеиных существ.


Гаруды - благородные, похожие на птиц существа, произошедшие от богов.
Они родились под лазурными небесами :ref:`Внешнего Плана <Planes--Outer-Sphere>` :ref:`Элизиума <plane--Elysium>`, но с тех пор перебрались на :ref:`Материальный План <plane--Material>`, и именно здесь они в основном и обитают.
На Голарионе самые большие популяции гаруд живут в суровых холмах и горах Вудры, хотя некоторые распространились в Невозможное Королевство Джалмерэй и даже в восточные горы Гарунда.

Гаруды обычно селятся в изолированных гнездовьях, но большую часть времени проводят планируя на ветру.
Многие гаруды кочуют или мигрируют, стремясь путешествовать и парить над новыми ландшафтами.
Хотя они остаются в стороне от гуманоидных обществ, они импульсивны и галантны, часто служат защитниками общин вблизи скал и горных вершин, где они устраивают свои гнезда.
Некоторые гаруды сочетают свою жажду странствий и добросердечие, служа гонцами и эмиссарами между отдаленными поселениями, однако большинство из них бросают эту работу через несколько лет, когда надоедает одна и та же местность.
Странствующие гаруды не менее сострадательны, и многие рассказывают о гарудах, которые пришли на помощь заблудившемуся путнику или перенесли в безопасное место душу, находящуюся в опасности.

Хотя гаруды высоко ценят большинство живых существ, они ненавидят змееподобных существ - особенно :doc:`наг </creatures/bestiary/Naga/index>`.
Редко когда их столкновение обходится без драки, и гаруда сойдет со своего пути, чтобы выследить нагу, если до него дойдет хотя бы малейший намек на слух о ее присутствии.

Оперение и окраска гаруды сильно варьируются в зависимости от предпочитаемой ими местности.
В лесах умеренного пояса гаруды могут быть похожи на соек или соколов, а тропические гаруды отличаются ярким оперением.


.. rst-class:: creature
.. _bestiary--Garuda:

Гаруда (`Garuda <https://2e.aonprd.com/Monsters.aspx?ID=1162>`_) / Существо 9
------------------------------------------------------------------------------------------------------------

- :alignment:`ХД`
- :size:`средний`
- небожитель

**Источник**: Bestiary 3 pg. 104

**Восприятие**: +20;
:ref:`ночное зрение <cr_ability--Darkvision>`

**Языки**: Небесный,
Всеобщий,
Вудранский

**Навыки**:
Атлетика +17,
Акробатика +21,
Запугивание +19,
Скрытность +19,
Выживание +16,
Религия +16

**Сил** +4,
**Лвк** +6,
**Тел** +4,
**Инт** +2,
**Мдр** +3,
**Хар** +4


**Предметы**:
*разящий композитный длинный лук +1* (100 стрел)

----------

**КБ**: 28;
**Стойкость**: +17,
**Рефлекс**: +21,
**Воля**: +16

**ОЗ**: 135

----------

**Скорость**: 25 футов,
полет 60 футов


**Ближний бой**: |д-1| клюв +21 [+16/+11] (:w_finesse:`точное`),
**Урон** 2d10+7 колющий + 1d6 добро

**Ближний бой**: |д-1| коготь +21 [+17/+13] (:w_agile:`быстрое`, :w_finesse:`точное`),
**Урон** 2d8+7 рубящий + 1d6 добро


**Дальний бой**: |д-1| *шоковый композитный длинный лук* +22 [+17/+12] (:w_deadly:`смертельное d10`, :w_volley:`залповое 30 фт`, шаг дистанции 100 фт, перезарядка 0, :t_magical:`магический`),
**Урон** 2d8+5 колющий + 1d6 электричество и 1d6 добро

**Электрические снаряды (Electric Projectiles)**
(:t_arcana:`аркана`, :t_evocation:`эвокация`)
Любая стрела, которую выстреливает гаруда получает эффекты :ref:`Шоковой (Shock) <item--Shock>` руны.


**Врожденные сакральные заклинания** КС 27

| **4 ур.** :ref:`spell--f--Freedom-of-Movement`
| **3 ур.** :ref:`spell--h--Haste`
| **2 ур.** :ref:`spell--s--See-Invisibility` (×3)
| **1 ур.** :ref:`spell--t--True-Strike` (по желанию)


**Ныряющее пикирование (Swooping Dive)** |д-2|
Гаруда летит по прямой линии вплоть до своей Скорости, снижаясь хотя бы на 10 футов, а затем совершает 2 Удара когтями.


**Ветряной порыв (Wind Blast)** |д-2|
(:t_evocation:`эвокация`, :t_air:`воздух`)
Гаруда взмахивает своими крыльями с огромной силой.
Это имеет те же эффекты, что и :ref:`spell--g--Gust-of-Wind` (КС 27) но с 30-футовым конусом.
В области с сыпучими частицами (на усмотрение Мастера), порывы от крыльев гаруды создают облако закрывающее обзор, делая всех существ в данной области :c_concealed:`скрытыми`, а существ вне области :c_concealed:`скрытыми` для находящихся внутри.





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst